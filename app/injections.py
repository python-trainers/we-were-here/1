from trainerbase.codeinjection import AllocatingCodeInjection
from memory import unityplayer, player_coords_pointer


update_player_coords_pointer = AllocatingCodeInjection(
    unityplayer.dll + 0xD111D4,
    f"""
        mov rax, {player_coords_pointer}
        mov [rax], r14

        mov rcx, [r14 + 0x1E8]
        movups [r14 + 0x1F0], xmm0
    """,
    original_code_length=15,
    is_long_x64_jump_needed=True,
)
