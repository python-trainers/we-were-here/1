from trainerbase.gui import add_teleport_to_gui, simple_trainerbase_menu
from teleport import tp


@simple_trainerbase_menu("We Were Here", 650, 300)
def run_menu():
    add_teleport_to_gui(tp, "Insert", "Home", "K")
