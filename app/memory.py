from types import SimpleNamespace

from pymem import process
from trainerbase.memory import POINTER_SIZE, pm

unityplayer_dll_base_address = process.module_from_name(pm.process_handle, "UnityPlayer.dll").lpBaseOfDll
unityplayer = SimpleNamespace(dll=unityplayer_dll_base_address)

player_coords_pointer = pm.allocate(POINTER_SIZE)
