from trainerbase.common import Teleport, Vector3
from objects import player_x, player_y, player_z


tp = Teleport(
    player_x,
    player_y,
    player_z,
    labels={
        'lib': (3.7725555896759033, -2.9064109325408936, -2.512493371963501),
        'start': (-2.20977783203125, 2.8392913341522217, 1.8854409456253052),
    },
    dash_coefficients=Vector3(0.3, 0.3, 0.3),
    minimal_movement_vector_length=0.16,
)
